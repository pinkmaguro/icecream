from django.urls import path

from .views import eat, hi

urlpatterns = [
    path('eat', eat),
    path('hi', hi),
]