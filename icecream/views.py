import random
from django.http import HttpResponse

def hi(request):
    num = random.randrange(0, 1000)
    text = f'<H1>Just hi  your lucky number is {num}</H1>'
    return HttpResponse(text)


def icecream_hi(request):
    num = random.randrange(0, 1000)
    text = f'<H1>Icecream hi  your lucky number is {num}</H1>'
    return HttpResponse(text)


def fallback(request):
    text = f'<H1>Fall back</H1>'
    return HttpResponse(text)